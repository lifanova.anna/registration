<?php
session_start();
if( isset($_SESSION['user'])){
  header('Location: ./profile.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Авторизация и регистрация </title>
<link rel="stylesheet" href="./assets/css/main.css">
</head>
<body>
  <!-- Форма авторизации -->
  <form action="./vendor/singin.php" method="post">
    <label> Login </label>
    <input type="text" placeholder="login" name="login">
    <label> Password </label>
    <input type="password" placeholder="password" name="password">
    <button type="submit"> Войти</button>
    <p>У вас нет аккаунта? <a href="/register.php"> Зарегистрируйтесь</a></p>
  </form>
  <?php  
  if (isset($_SESSION['message'])) {
    echo '<p class="msg">' . $_SESSION['message'] . '</p>';
    unset($_SESSION['message']);
  }
  ?>
</body>
</html>