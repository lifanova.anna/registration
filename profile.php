<?php
session_start();
if( !$_SESSION['user']){
  header('Location: ./index.php');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Авторизация и регистрация </title>
  <link rel="stylesheet" href="./assets/css/main.css">
</head>

<body>
  <!-- Форма авторизации -->
  <div class="profile">
    <img src="<?= $_SESSION['user']['avatar'] ?>" width="100" alt="">
    <h2><?= $_SESSION['user']['fullname'] ?></h2>
    <a href=""><?= $_SESSION['user']['email'] ?></a>
  </div>
    <a href="./vendor/logout.php" class="logout">Выход</a>

</body>

</html>