<?php 
session_start(); 

// Параметры приложения
$clientId     = '8175080'; // ID приложения
$clientSecret = 'WLQobjxeaohAPVhDmitB'; // Защищённый ключ
$redirectUri  = 'http://localhost/vendor/singin.php'; // Адрес, на который будет переадресован пользователь после прохождения авторизации
// $redirectUri  = 'http://localhost/_blog/tests/vk-api/oauth.php'; // Адрес, на который будет переадресован пользователь после прохождения авторизации

// Формируем ссылку для авторизации
$params = array(
	'client_id'     => $clientId,
	'redirect_uri'  => $redirectUri,
	'response_type' => 'code',
	'v'             => '5.74', // (обязательный параметр) версия API, которую Вы используете https://vk.com/dev/versions

	// Права доступа приложения https://vk.com/dev/permissions
	// Если указать "offline", полученный access_token будет "вечным" (токен умрёт, если пользователь сменит свой пароль или удалит приложение).
	// Если не указать "offline", то полученный токен будет жить 12 часов.
	'scope'         => 'photos,offline',
);

// Выводим на экран ссылку для открытия окна диалога авторизации
echo '<a href="http://oauth.vk.com/authorize?' . http_build_query( $params ) . '">Авторизация через ВКонтакте</a>';