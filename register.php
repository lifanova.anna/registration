<?php
session_start();
if( isset($_SESSION['user'])){
  header('Location: ./profile.php');
}
?>

<!DOCTYPE html>
<html lang="ru">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Авторизация и регистрация </title>
  <link rel="stylesheet" href="./assets/css/main.css">
</head>

<body>
  <!-- Форма авторизации -->
  <form action="./vendor/singup.php" method="post" enctype="multipart/form-data">
    <label> ФИО </label>
    <input type="text" placeholder="Логин" name="fullname">
    <label> Login </label>
    <input type="text" placeholder="Логин" name="login">
    <label> Email </label>
    <input type="email" placeholder="Email" name="email">
    <label> Изображение профиля </label>
    <input type="file" name="avatar">
    <label> Password </label>
    <input type="password" placeholder="Пароль" name="password">
    <label> Password </label>
    <input type="password" placeholder="Подтверждение пароля" name="password_confirm">

    <button> Войти</button>
    <button type="button" id="vkLogin">Войти через ВКонтакте</button>
    <p>У вас уже аккаунт? <a href="/index.php"> Авторизируйтесь</a></p>
  </form>
  <?php
  if (isset($_SESSION['message'])) {
    echo '<p class="msg">' . $_SESSION['message'] . '</p>';
    unset($_SESSION['message']);

  }
  ?>
</body>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://vk.com/js/api/openapi.js?154" type="text/javascript"></script>
<script src="./assets/app/main.js" type="text/javascript"></script>
</html>